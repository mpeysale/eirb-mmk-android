package com.eirbmmk.app.connection;

import android.os.AsyncTask;

import com.eirbmmk.app.BaseController;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.Screen;
import com.eirbmmk.app.Token;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

/**
 * This class correspond to the controller part of the MVC design pattern for the connection
 * functionality of the application.
 */
public class ConnectionController extends BaseController {

    /* -------------------- Attributes -------------------- */


    /**
    * Model part of the MVC design pattern
    */
    private ConnectionModel mModel;

    /**
    * View part of the MVC design pattern
    */
    private ConnectionFragment mFragment;

    /* -------------------- Constructors -------------------- */


    /**
     * Constructor of ConnectionController.
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model for the Connection functionality
     * @param fragment the view of the connection functionality
     */
    public ConnectionController(MainActivityController container, ConnectionModel model, ConnectionFragment fragment)
    {
        super(container);
        mModel = model;
        mFragment = fragment;
    }


    /* -------------------- Methods -------------------- */


    /**
     * Call the ConnectionModel to get the authorization URL
     *
     * @return the authorization URL
     */
    public String getAuthorizationUrl()
    {
        return mModel.getAuthorizationUrl();
    }

    /**
     * Sends POST HTTP Request to get the access token from eirb.fr
     * If it succeeds, access to eirb.fr web-services to get user's login.
     *
     * @param code the authorization code sent back by eirb.fr after the user logs in on the website.
     */
    private void getAccessToken(String code)
    {
        AsyncTask<String, Void, String> accessTokenRequest = new AsyncTask<String, Void, String>() {

            private DefaultHttpClient mHttpClient;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                // Disable SSL verification problems
                HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
                DefaultHttpClient client = new DefaultHttpClient();

                org.apache.http.conn.scheme.SchemeRegistry registry = new org.apache.http.conn.scheme.SchemeRegistry();
                org.apache.http.conn.ssl.SSLSocketFactory socketFactory = org.apache.http.conn.ssl.SSLSocketFactory.getSocketFactory();
                socketFactory.setHostnameVerifier((org.apache.http.conn.ssl.X509HostnameVerifier) hostnameVerifier);
                registry.register(new org.apache.http.conn.scheme.Scheme("https", socketFactory, 443));
                org.apache.http.impl.conn.SingleClientConnManager mgr = new org.apache.http.impl.conn.SingleClientConnManager(client.getParams(), registry);
                mHttpClient = new DefaultHttpClient(mgr, client.getParams());

                HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);


            }

            @Override
            protected String doInBackground(String... params) {
                String authCode = params[0];
                String jsonResult = null;
                if (!isCancelled()) {
                    // HTTP POST Creation
                    HttpPost httpPost = new HttpPost(mModel.getTokenBaseUrl());
                    HttpResponse httpResponse = null;

                    httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

                    // Set HTTP POST content
                    try {
                        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                        nameValuePairs.add(new BasicNameValuePair("grant_type", "authorization_code"));
                        nameValuePairs.add(new BasicNameValuePair("code", authCode));
                        nameValuePairs.add(new BasicNameValuePair("redirect_uri", mModel.getCallbackUrl()));
                        nameValuePairs.add(new BasicNameValuePair("client_id", mModel.getClientID()));
                        nameValuePairs.add(new BasicNameValuePair("client_secret", mModel.getClientSecret()));
                        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                    } catch (Exception e) {
                        android.util.Log.i("ERROR", "Error setting HTTP POST token request content.");
                        return null;
                    }

                    // Sends HTTP POST Request
                    try {
                        httpResponse = mHttpClient.execute(httpPost);

                        // get HTTP POST result
                        jsonResult = EntityUtils.toString(httpResponse.getEntity());
                    } catch (Exception e) {
                        android.util.Log.e("ERROR", e.toString());
                        e.printStackTrace();
                    }
                }

                return jsonResult;
            }

            @Override
            protected void onPostExecute(String jsonResult) {
                String accessToken = null;
                String refreshToken = null;
                String tokenType = null;
                Calendar expirationDate = Calendar.getInstance(); // set to current date
                Token token = null;

                if (jsonResult == null)
                {
                    mFragment.hideLoadingSpinner();
                    mFragment.displayErrorMessage("Une erreur est survenue lors de l'authentification. Veuillez réessayer plus tard.");
                }

                try {
                    JSONObject jsonObject = new JSONObject(jsonResult);
                    accessToken = jsonObject.getString("access_token");
                    refreshToken = jsonObject.getString("refresh_token");
                    tokenType = jsonObject.getString("token_type");
                    int seconds = Integer.parseInt(jsonObject.getString("expires_in"));
                    expirationDate.add(Calendar.SECOND, seconds);

                    token = new Token(accessToken, refreshToken, tokenType, expirationDate);

                } catch (JSONException e) {
                    android.util.Log.i("ERROR", "Erreur lors du parsage du JSON contenant le token.");

                    mFragment.hideLoadingSpinner();
                    mFragment.displayErrorMessage("Une erreur est survenue lors de l'authentification. Veuillez réessayer plus tard.");
                }

                // save the token
                mMainActivityController.saveToken(token);

                // delete cookies stored by the webview
                mFragment.deleteCookies();

                // get user's login
                getUserLogin(token);

                // remove the task to running tasks list
                mAsyncRunningTasks.remove(this);
            }
        };

        // add the task to running tasks list
        mAsyncRunningTasks.add(accessTokenRequest);

        // start the async task
        accessTokenRequest.execute(code);
    }

    /**
     * Sends an GET HTTP request signed with the token given in parameter to get user's login.
     * If it succeeds, the user is fully authenticated in the application and the method switch
     * the application to authenticated mode.
     *
     * @param token the token to use to signed / secured the GET HTTP request
     */
    private void getUserLogin(final Token token)
    {
        AsyncTask<Void, Void, String> loginRequest = new AsyncTask<Void, Void, String>() {

            private DefaultHttpClient mHttpClient;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                    // create HTTP Client
                    mHttpClient = new DefaultHttpClient();

            }

            @Override
            protected String doInBackground(Void... params) {
                String jsonResult = null;

                if (!isCancelled()) {
                    // HTTP GET request Creation
                    HttpGet httpPost = new HttpGet("http://auth.eirb.fr/oauth/user/?access_token=" + token.getAccessToken());
                    HttpResponse httpResponse = null;

                    httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

                    // Sends HTTP GET Request
                    try {
                        httpResponse = mHttpClient.execute(httpPost);

                        // get HTTP GET request result
                        jsonResult = EntityUtils.toString(httpResponse.getEntity());
                    } catch (Exception e) {
                        android.util.Log.e("ERROR", e.toString());
                        e.printStackTrace();
                    }
                }

                return jsonResult;
            }

            @Override
            protected void onPostExecute(String jsonResult) {
                String username = null;

                if (jsonResult == null)
                {
                    mFragment.hideLoadingSpinner();
                    mFragment.displayErrorMessage("Une erreur est survenue lors de l'authentification. Veuillez réessayer plus tard.");
                }

                try {
                    JSONObject jsonObject = new JSONObject(jsonResult);
                    username = jsonObject.getString("username");
                } catch (JSONException e) {
                    android.util.Log.i("ERROR", "Erreur lors du parsage du JSON contenant le login utilisateur.");

                    mFragment.hideLoadingSpinner();
                    mFragment.displayErrorMessage("Une erreur est survenue lors de l'authentification. Veuillez réessayer plus tard.");
                }

                switchToAuthenticatedMode(username);

                // remove the task to running tasks list
                mAsyncRunningTasks.remove(this);
            }
        };

        // add the task to running tasks list
        mAsyncRunningTasks.add(loginRequest);

        //start the async task
        loginRequest.execute();
    }

    /**
     * Finish the authentication procedure after the user successfully logs in on eirb.fr.
     * It sends POST HTTP requests in order to get the access token from eirb.fr and then user login.
     *
     * @param code the authorization code sent back by eirb.fr after the user logs in on the website.
     * @return true if the authentication successfully finished, false otherwise.
     */
    public void finishAuthentication(final String code)
    {
        mFragment.showLoadingSpinner();

        getAccessToken(code);
    }

    /**
     * Does different needed things to switch the application in authenticated mode.
     *
     * @param username the username of user's account with which the user is authenticated
     */
    private void switchToAuthenticatedMode(String username)
    {
        mFragment.hideLoadingSpinner();

        mFragment.displayConnectionSuccess();

        // save user's login
        mMainActivityController.saveUserLogin(username);

        // User is fully authenticated, switch application to authenticated mode
        mMainActivityController.setAuthenticatedMode();
    }
}
