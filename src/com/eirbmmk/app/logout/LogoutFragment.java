package com.eirbmmk.app.logout;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.eirbmmk.app.BaseFragment;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.R;

public class LogoutFragment extends BaseFragment {

	 /* -------------------- Attributes -------------------- */

    /**
     * The controller part of the MVC Clubs pattern
     */
    private LogoutController mController;


    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public LogoutFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }



    /**
     * Constructor of the LogoutFragment class
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model including logout information.
     */
    public LogoutFragment(MainActivityController container, LogoutModel model)
    {
        mController = new LogoutController(container, model, this);
        super.setBaseController(mController);
    }

    /* -------------------- Methods -------------------- */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // If the fragment is in an illegal state, don't continue, wait the recreation of it.
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;

        mRootView = inflater.inflate(R.layout.fragment_logout, container, false);

        TextView username = (TextView) mRootView.findViewById(R.id.logout_username_textview);
        Button logoutButton = (Button) mRootView.findViewById(R.id.logout_button);

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mController.logoutUser();
            }
        });

        username.setText(mController.getUserLogin());

        return mRootView;
    }

    /**
     * Displays logout success message
     */
    public void displayLogoutSuccess()
    {
        mRootView.findViewById(R.id.logout_username_textview).setVisibility(View.GONE);
        mRootView.findViewById(R.id.logout_button).setVisibility(View.GONE);
        TextView sentence = (TextView) mRootView.findViewById(R.id.logout_connected_sentence);

        sentence.setTypeface(Typeface.DEFAULT_BOLD);
        sentence.setGravity(Gravity.CENTER);

        sentence.setText("Déconnexion réussie !");
    }
}

