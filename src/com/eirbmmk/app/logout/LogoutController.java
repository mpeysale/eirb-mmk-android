package com.eirbmmk.app.logout;

import com.eirbmmk.app.BaseController;
import com.eirbmmk.app.MainActivityController;

/**
 * This class correspond to the controller part of the MVC design pattern for the user logout
 * functionality of the application.
 */
public class LogoutController extends BaseController {

    /* -------------------- Attributes -------------------- */


    /**
    * Model part of the MVC design pattern
    */
    private LogoutModel mModel;

    /**
    * View part of the MVC design pattern
    */
    private LogoutFragment mFragment;


      /* -------------------- Constructors -------------------- */


    /**
     * Constructor of LogoutController.
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model for the logout functionality
     * @param fragment the view of the logout functionality
     */
    public LogoutController(MainActivityController container, LogoutModel model, LogoutFragment fragment)
    {
        super(container);
        mModel = model;
        mFragment = fragment;
    }


    /* -------------------- Methods -------------------- */

    /**
     * Get the login of the authenticated user.
     * If the user is not authenticated, return null;
     *
     * @return return the login of the authenticated user or null if the user is not authenticated.
     */
    public String getUserLogin()
    {
        return mMainActivityController.getUserLogin();
    }


    /**
     * Logout the User and displays non authenticated mode.
     */
    public void logoutUser()
    {
        mMainActivityController.logoutUser();

        mFragment.displayLogoutSuccess();

        mMainActivityController.setNonAuthenticatedMode();
    }
}
