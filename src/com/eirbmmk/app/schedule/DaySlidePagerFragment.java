package com.eirbmmk.app.schedule;

import android.app.Fragment;
import android.os.Bundle;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.eirbmmk.app.MainActivity;
import com.eirbmmk.app.R;

/**
 * Fragment used for each page of the ViewPager
 */
public class DaySlidePagerFragment extends Fragment {

    /**
     * The controller part of the MVC News pattern
     */
    private ScheduleController mController;

    /**
     * The current day to display
     */
    private Time mCurrentDay;

    /**
     * The rootView of the fragment layout
     */
    private View mRootView;

    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public DaySlidePagerFragment(){
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }

    /**
     *  Constructor used to instantiate the current day when the user slide the view
     * @param currentDay
     * @param controller
     */
    public DaySlidePagerFragment(Time currentDay, ScheduleController controller)
    {
        mCurrentDay = currentDay;
        mController = controller;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        if (savedInstanceState != null) {
//            // the OS destroyed the old fragment and instantiates
//            // a new one using the default constructor. This new fragment has an illegal state
//            // (no controller and mainActivityController reference).
//            // So recreate all the MVC pattern for the fragment.
//            // Warning : If the application was completely killed by the OS, the current Fragment
//            // will be the home fragment and not this fragment.
//
//            MainActivity mainAc = (MainActivity) getActivity();
//            mainAc.recreateCurrentMVCFragment();
//            return null;
//        }
        android.util.Log.i("TEST", "Fragment instancié");
        mRootView = inflater.inflate(R.layout.fragment_day_slide_pager, container, false);


        ScrollView scrollView = (ScrollView) mRootView.findViewById(R.id.day_slide_pager_scrollview);
        DayViewRenderer view = new DayViewRenderer(container.getContext(), mCurrentDay, mController);
        scrollView.addView(view);

        return mRootView;
    }
}
