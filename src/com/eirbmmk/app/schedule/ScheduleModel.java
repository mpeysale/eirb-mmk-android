package com.eirbmmk.app.schedule;


import android.text.format.Time;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;




/**
 * Contains all the needed information for Schedule functionality like Schedule webservices URL.
 */
public class ScheduleModel {

    /* -------------------- Attributes -------------------- */

    private ArrayList<Course> mCourses = new ArrayList<Course>();

    private String mScheduleURL = "http://edt.eirb.fr/";
    /* -------------------- Constructors -------------------- */

    public ScheduleModel() {
    }
    /* -------------------- Methods -------------------- */

    /**
     * parse and store the Data in the Json in an ArrayList
     * @param login of the user
     */
    public void fetchData(String login){
        // Get Data from Server
        GetJson jParser = new GetJson();
        // get JSON data from URL
        String rawJSON = jParser.getJSONFromUrl(mScheduleURL + login + ".json");

        // Store it in ArrayList
        try {
            JSONObject jObject = new JSONObject(rawJSON);
            JSONArray jsonArray = jObject.getJSONArray("events");
            JSONObject courseObj;
            Time currentDate_from;
            Time currentDate_to;

            for(int cursor = 0; cursor < jsonArray.length(); cursor++){
                courseObj = new JSONObject(jsonArray.getString(cursor));
                currentDate_from = new Time();
                currentDate_from.parse3339(courseObj.getString("date_from"));
                currentDate_from.normalize(false);
                currentDate_to = new Time();
                currentDate_to.parse3339(courseObj.getString("date_to"));
                currentDate_from.switchTimezone(Time.getCurrentTimezone());
                currentDate_to.switchTimezone(Time.getCurrentTimezone());
                currentDate_to.normalize(false);
                    String teachersString = "";
                    JSONArray teachers = courseObj.getJSONArray("staff");
                    for (int i = 0 ; i < teachers.length() ; i++){
                        teachersString += teachers.get(i).toString();
                        if (i != teachers.length()-1){
                            teachersString += ", ";
                        }
                    }

                    String locationsString = "";
                    JSONArray rooms = courseObj.getJSONArray("rooms");
                    for (int i = 0 ; i < rooms.length() ; i++){
                        locationsString += rooms.get(i).toString();
                        if (i != rooms.length()-1){
                            locationsString += ", ";
                        }
                    }
                    JSONArray groups = courseObj.getJSONArray("groups");
                    String groupString = "";
                    for(int i=0; i<groups.length(); i++){
                        groupString += groups.get(i).toString();
                        if (i != groups.length()-1){
                            groupString += ", ";
                        }
                    }
                    Course c = new Course(courseObj.getString("title"), courseObj.getString("type"), groupString, teachersString, locationsString, currentDate_from, currentDate_to);
                    mCourses.add(c);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     *
     * @param currentDay
     * @return the courses of the currentDay
     */
    public ArrayList<Course> getListEvent(Time currentDay){
        ArrayList<Course> todayCourses = new ArrayList<Course>();
        for(Course c : mCourses){
            if(c.getStartTime().monthDay == currentDay.monthDay){
                todayCourses.add(c);
            }
        }
        return todayCourses;
    }

}
