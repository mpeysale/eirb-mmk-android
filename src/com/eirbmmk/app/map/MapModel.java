package com.eirbmmk.app.map;

import android.content.Context;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.util.Log;

import com.eirbmmk.app.R;
import com.eirbmmk.app.schedule.GetJson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static com.eirbmmk.app.map.MapFragment.getFragmentContext;

/**
 * Contains all the needed information for Map functionality like SVG files paths.
 */
public class MapModel {

       /* -------------------- Global Variables -------------------- */


    /* -------------------- Attributes -------------------- */
    private static Context mContext;

    /* -------------------- Constructors -------------------- */

    public MapModel()
    {

    }

    public static Context getContext() {
        return mContext;
    }

    public static void setContext(Context mContext) {
        MapModel.mContext = mContext;
    }


    /* -------------------- Methods -------------------- */


}
