package com.eirbmmk.app.map;

import com.eirbmmk.app.BaseFragment;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.MainActivityModel;
import com.eirbmmk.app.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.xml.sax.SAXException;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

public class MapFragment extends BaseFragment{


	
    /* -------------------- Attributes -------------------- */

    /**
     * The controller part of the MVC Map pattern
     */
    private MapController mController;

    private static Context mContext;

    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public MapFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }


    /**
     * Constructor of the MapFragment class
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model including Map information.
     */
    public MapFragment(MainActivityController container, MapModel model)
    {
        mController = new MapController(container, model, this);
        super.setBaseController(mController);
    }

    /**
     * public method to get the context from the fragment
     * @return the application context
     */
    public static Context getFragmentContext() {
        return mContext;
    }

    /* -------------------- Methods -------------------- */

    public Context getContext(){
        return  mContext;
    }

    class JsObject {
        @JavascriptInterface

        public void roomClicked(String room){
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle(room);
            builder.setPositiveButton("OK", null);

            AlertDialog alertDialog;
            alertDialog = builder.create();
            alertDialog.show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // If the fragment is in an illegal state, don't continue, wait the recreation of it.
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;

        mContext = container.getContext();
        mController.setContext(mContext);
        mRootView = inflater.inflate(R.layout.fragment_map, container, false);
        if(!mController.isNetworkConnected())
            mRootView = inflater.inflate(R.layout.no_internet_connection,container, false);
        else {
            final WebView mapWebView = (WebView) mRootView.findViewById(R.id.mapWebView);
            mapWebView.loadUrl("file:///android_asset/HTML/Map/index.html");
            WebSettings settings = mapWebView.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setAllowUniversalAccessFromFileURLs(true);
            settings.setBuiltInZoomControls(true);
            mapWebView.addJavascriptInterface(new JsObject(), "JavaMethods");
            mapWebView.loadUrl("javascript:auth = " + (mController.isLogged()? "true": "false"));


            Button ss_button = (Button) mRootView.findViewById(R.id.button_ss);
            ss_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mController.isLogged()){
                        mapWebView.loadUrl("javascript:loadFloorAuth('ss')");
                    } else {
                        mapWebView.loadUrl("javascript:loadFloor('ss')");
                    }
                }
            });

            Button rdc_button = (Button) mRootView.findViewById(R.id.button_rdc);
            rdc_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mController.isLogged()){
                        mapWebView.loadUrl("javascript:loadFloorAuth('rdc')");
                    } else {
                        mapWebView.loadUrl("javascript:loadFloor('rdc')");
                    }
                }
            });

            Button r1_button = (Button) mRootView.findViewById(R.id.button_r1);
            r1_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mController.isLogged()){
                        mapWebView.loadUrl("javascript:loadFloorAuth('etage1')");
                    } else {
                        mapWebView.loadUrl("javascript:loadFloor('etage1')");
                    }
                }
            });

            Button r2_button = (Button) mRootView.findViewById(R.id.button_r2);
            r2_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mController.isLogged()){
                        mapWebView.loadUrl("javascript:loadFloorAuth('etage2')");
                    } else {
                        mapWebView.loadUrl("javascript:loadFloor('etage2')");
                    }
                }
            });

            Button r3_button = (Button) mRootView.findViewById(R.id.button_r3);
            r3_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mController.isLogged()){
                        mapWebView.loadUrl("javascript:loadFloorAuth('etage3')");
                    } else {
                        mapWebView.loadUrl("javascript:loadFloor('etage3')");
                    }
                }
            });

        }
        return mRootView;
    }


}
