package com.eirbmmk.app.events;

import android.content.Context;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eirbmmk.app.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by maelorn on 01/04/14.
 */
public class CalendarAdapter extends BaseAdapter{
    static final int FIRST_DAY_OF_WEEK =0;
    Context context;
    Calendar cal;
    public String[] days;
    ArrayList<Time> dayList = new ArrayList<Time>();

    public CalendarAdapter(Context context, Calendar cal){
        this.cal = cal;
        this.context = context;
        cal.set(Calendar.DAY_OF_MONTH, 1);
        refreshDays();
    }

    @Override
    public int getCount() {
        return days.length;
    }

    @Override
    public Object getItem(int position) {
        return dayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public int getMonth(){
        return cal.get(Calendar.MONTH);
    }


    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View v = view;
        LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(position >= 0 && position < 7){
            Log.i("LOL", "DANS - 7");
            v = vi.inflate(R.layout.day_of_week, null);
            TextView day = (TextView)v.findViewById(R.id.textView1);

            if(position == 0){
                day.setText(R.string.sunday);
            }else if(position == 1){
                day.setText(R.string.monday);
            }else if(position == 2){
                day.setText(R.string.tuesday);
            }else if(position == 3){
                day.setText(R.string.wednesday);
            }else if(position == 4){
                day.setText(R.string.thursday);
            }else if(position == 5){
                day.setText(R.string.friday);
            }else if(position == 6){
                day.setText(R.string.saturday);
            }

        } else{
            Log.i("LOL", "+ 7");
            v = vi.inflate(R.layout.day_view, null);
            Time day = dayList.get(position);
            TextView dayTV = (TextView)v.findViewById(R.id.date);
            RelativeLayout rl = (RelativeLayout)v.findViewById(R.id.rl);
            if(day == null){
                rl.setVisibility(View.GONE);
            }else{
                dayTV.setVisibility(View.VISIBLE);
                dayTV.setText(day.format("%d"));
            }
        }
        return v;
    }

    public void refreshDays(){
        dayList.clear();
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH)+7;
        int firstDay = cal.get(Calendar.DAY_OF_WEEK);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        TimeZone tz = TimeZone.getDefault();

        if(firstDay==1){
            days = new String[lastDay+(FIRST_DAY_OF_WEEK*6)];
        }
        else {
            days = new String[lastDay+firstDay-(FIRST_DAY_OF_WEEK+1)];
        }

        int j=FIRST_DAY_OF_WEEK;

        if(firstDay>1) {
            for(j=0;j<(firstDay-FIRST_DAY_OF_WEEK)+7;j++) {
                days[j] = "";
                Time d = null;
                dayList.add(d);
            }
        }
        else {
            for(j=0;j<(FIRST_DAY_OF_WEEK*6)+7;j++) {
                days[j] = "";
                Time d = null;
                dayList.add(d);
            }
            j=FIRST_DAY_OF_WEEK*6+1; // sunday => 1, monday => 7
        }

        int dayNumber = 1;

        if(j>0 && dayList.size() > 0 && j != 1){
            dayList.remove(j-1);
        }

        for(int i=j-1;i<days.length;i++) {
            Time d = new Time();
            d.monthDay = dayNumber;
            d.month = month;
            d.year = year;
            d.normalize(false);

            //Calendar cTemp = Calendar.getInstance();
            //cTemp.set(year, month, dayNumber);
            //int startDay = Time.getJulianDay(cTemp.getTimeInMillis(), TimeUnit.MILLISECONDS.toSeconds(tz.getOffset(cTemp.getTimeInMillis())));

            //d.setAdapter(this);
            //d.setStartDay(startDay);

            days[i] = ""+dayNumber;
            dayNumber++;
            dayList.add(d);
        }
    }



}

