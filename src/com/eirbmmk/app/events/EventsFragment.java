package com.eirbmmk.app.events;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import com.eirbmmk.app.BaseFragment;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.R;


public class EventsFragment extends BaseFragment {
	
 /* -------------------- Attributes -------------------- */

    private static final int focusedMontColor = 0xff333333;
    private static final int unFocusedMontColor = 0x00333333;
    /**
     * The controller part of the MVC Events pattern
     */
    private EventsController mController;


    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public EventsFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }



    /**
     * Constructor of the EventsFragment class
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model including events information.
     */
    public EventsFragment(MainActivityController container, EventsModel model)
    {
        mController = new EventsController(container, model, this);
        super.setBaseController(mController);
    }

    /* -------------------- Methods -------------------- */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // If the fragment is in an illegal state, don't continue, wait the recreation of it.
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;
        mRootView = inflater.inflate(R.layout.fragment_events, container, false);


        return mRootView;

        /* ------ Uncomment to continue development ------- */

//        mRootView = inflater.inflate(R.layout.fragment_events, container, false);
//        return mRootView;
    }
}
