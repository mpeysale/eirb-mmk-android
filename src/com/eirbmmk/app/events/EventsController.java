package com.eirbmmk.app.events;

import com.eirbmmk.app.BaseController;
import com.eirbmmk.app.MainActivityController;

/**
 * This class correspond to the controller part of the MVC design pattern for Events
 * functionality of the application.
 */
public class EventsController extends BaseController {

    /* -------------------- Attributes -------------------- */


    /**
    * Model part of the MVC design pattern
    */
    private EventsModel mModel;

    /**
    * View part of the MVC design pattern
    */
    private EventsFragment mFragment;


      /* -------------------- Constructors -------------------- */


    /**
     * Constructor of EventsController.
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model for the Events functionality
     * @param fragment the view of the Events functionality
     */
    public EventsController(MainActivityController container, EventsModel model, EventsFragment fragment)
    {
        super(container);
        mModel = model;
        mFragment = fragment;
    }


    /* -------------------- Methods -------------------- */
}
