package com.eirbmmk.app.example;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.eirbmmk.app.BaseFragment;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.R;

public abstract class ExampleFragment extends BaseFragment {

	 /* -------------------- Attributes -------------------- */

    /**
     * The controller part of the MVC Clubs pattern
     */
    private ExampleController mController;


    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public ExampleFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }



    /**
     * Constructor of the ExampleFragment class
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model including clubs information.
     */
    public ExampleFragment(MainActivityController container, ExampleModel model)
    {
        mController = new ExampleController(container, model, this);
        super.setBaseController(mController);
    }

    /* -------------------- Methods -------------------- */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // If the fragment is in an illegal state, don't continue, wait the recreation of it.
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;

        /* ------ To test internet connection ------ */
//        if (!mController.isNetworkConnected()){
//            mRootView = inflater.inflate(R.layout.no_internet_connection, container, false);
//            return mRootView;
//        }

        mRootView = inflater.inflate(R.layout.fragment_news, container, false);
        return mRootView;
    }
}

