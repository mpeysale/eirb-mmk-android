package com.eirbmmk.app.example;

import com.eirbmmk.app.BaseController;
import com.eirbmmk.app.MainActivityController;

/**
 * This class correspond to the controller part of the MVC design pattern for the Example
 * functionality of the application.
 */
public class ExampleController extends BaseController {

    /* -------------------- Attributes -------------------- */


    /**
    * Model part of the MVC design pattern
    */
    private ExampleModel mModel;

    /**
    * View part of the MVC design pattern
    */
    private ExampleFragment mFragment;


      /* -------------------- Constructors -------------------- */


    /**
     * Constructor of ExampleController.
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model for the Example functionality
     * @param fragment the view of the Example functionality
     */
    public ExampleController(MainActivityController container, ExampleModel model, ExampleFragment fragment)
    {
        super(container);
        mModel = model;
        mFragment = fragment;
    }


    /* -------------------- Methods -------------------- */
}
