package com.eirbmmk.app.profiles;

import com.eirbmmk.app.BaseController;
import com.eirbmmk.app.schedule.ScheduleFragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.NetworkOnMainThreadException;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.Screen;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * This class correspond to the controller part of the MVC design pattern for Profiles
 * functionality.
 */
public class ProfilesController extends BaseController {

    /* -------------------- Attributes -------------------- */


    /**
    * Model part of the MVC design pattern
    */
    private ProfilesModel mModel;

    /**
    * View part of the MVC design pattern
    */
    private ProfilesFragment mFragment;


      /* -------------------- Constructors -------------------- */


    /**
     * Constructor of ProfilesController class.
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model for the Profiles functionality
     * @param fragment the view of the Profiles functionality
     */
    public ProfilesController(MainActivityController container, ProfilesModel model, ProfilesFragment fragment)
    {
        super(container);
        mModel = model;
        mFragment = fragment;
    }


    /* -------------------- Methods -------------------- */



}
