package com.eirbmmk.app.profiles;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.eirbmmk.app.BaseFragment;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class ProfilesFragment extends BaseFragment {

	 /* -------------------- Attributes -------------------- */

    /**
     * The controller part of the MVC Connection pattern
     */
    private ProfilesController mController;


    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public ProfilesFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }

    /**
     * Constructor of the ProfilesFragment class
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model including profiles information.
     */
    public ProfilesFragment(MainActivityController container, ProfilesModel model)
    {
        mController = new ProfilesController(container, model, this);
        super.setBaseController(mController);
    }

    /* -------------------- Methods -------------------- */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // If the fragment is in an illegal state, don't continue, wait the recreation of it.
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;



        /* ------ Uncomment to continue development ------- */

        mRootView = inflater.inflate(R.layout.fragment_profiles, container, false);

        if(!mController.isNetworkConnected()){
            mRootView = inflater.inflate(R.layout.no_internet_connection, container, false);
        }
        else {
            // Do something
        }

        return mRootView;
    }


}
