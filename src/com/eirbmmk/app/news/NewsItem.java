package com.eirbmmk.app.news;

import java.util.Date;

/**
 * Interface for storing News in an ArrayList
 */
public abstract class NewsItem {

    /* -------------------- Attributes -------------------- */

    /**
     * News date
     */
    protected Date mNewsDate = null;

    /**
     * News content
     */
    protected String mContent = null;



	/* -------------------- Constructors -------------------- */

    /**
     * Constructor of NewsItems
     * @param content news content
     * @param date news date
     */
    public NewsItem(String content, Date date)
    {
        mContent = content;
        mNewsDate = date;
    }

    /* -------------------- Methods -------------------- */


    /**
     * Returns news date
     * @return the date of the news
     */
    public Date getNewsDate()
    {
        return mNewsDate;
    }

//    public void setDate(Date newsDate) {
//        this.mNewsDate = newsDate;
//    }

    /**
     * Returns news content
     * @return the content
     */
    public String getContent() { return mContent; }

//    public void setContent(String content) { mContent = content; }

}
