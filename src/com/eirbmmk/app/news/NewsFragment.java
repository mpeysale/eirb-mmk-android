package com.eirbmmk.app.news;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.eirbmmk.app.BaseFragment;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.R;

import java.util.ArrayList;

public class NewsFragment extends BaseFragment {

	 /* -------------------- Attributes -------------------- */

    /**
     * The controller part of the MVC News pattern
     */
    private NewsController mController;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ListView mListView;



    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public NewsFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }



    /**
     * Constructor of the NewsFragment class
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model including news information.
     */
    public NewsFragment(MainActivityController container, NewsModel model) {
        mController = new NewsController(container, model, this);

        super.setBaseController(mController);
    }

    /* -------------------- Methods -------------------- */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // If the fragment is in an illegal state, don't continue, wait the recreation of it.
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;

        mRootView = inflater.inflate(R.layout.fragment_news, container, false);

        if(!mController.isNetworkConnected()){
            mRootView = inflater.inflate(R.layout.no_internet_connection, container, false);
        } else {
            showLoadingSpinner();
            mController.getNews();

            mSwipeRefreshLayout = ((SwipeRefreshLayout) mRootView.findViewById(R.id.swipe_container));
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

                @Override
                public void onRefresh() {
                    // get the new data from you data source
                    // TODO : request data here
                    mController.getNews();
                    // our swipeRefreshLayout needs to be notified when the data is returned in order for it to stop the animation
                }

            });

            //TODO Replace with IPB colors
            mSwipeRefreshLayout.setColorScheme(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);

            mListView = (ListView) mRootView.findViewById(R.id.newsListView);
            mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem,
                                     int visibleItemCount, int totalItemCount) {
                    boolean enable = false;
                    if(mListView != null && mListView.getChildCount() > 0){
                        // check if the first item of the list is visible
                        boolean firstItemVisible = mListView.getFirstVisiblePosition() == 0;
                        // check if the top of the first item is visible
                        boolean topOfFirstItemVisible = mListView.getChildAt(0).getTop() == 0;
                        // enabling or disabling the refresh layout
                        enable = firstItemVisible && topOfFirstItemVisible;
                    }
                    mSwipeRefreshLayout.setEnabled(enable);
                }
            });

        }
        return mRootView;
    }





    /**
     * Puts News in a ListView through an ArrayAdapter
     *
     * @param news ArrayList of NewsItem containing the News to display
     */
    public void displayNews(final ArrayList<NewsItem> news){
        NewsListAdapter adapter = new NewsListAdapter(mRootView.getContext(), news);

        mListView.setAdapter(adapter);
        mListView.invalidate();
        mSwipeRefreshLayout.setRefreshing(false);
        mListView.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mController.displayDetailedNewsFragment(news.get(position));
            }
        });
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return mSwipeRefreshLayout;
    }

}