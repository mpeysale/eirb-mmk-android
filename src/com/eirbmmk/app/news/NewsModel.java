package com.eirbmmk.app.news;

import android.util.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;


/**
 * Contains all the needed information for getting mNews 
 */
public class NewsModel {

    private static final String mSchoolNewsURL = "http://appli.eirbmmk.fr/News";

    /* -------------------- Attributes -------------------- */

    private ArrayList<NewsItem> mNews = new ArrayList<NewsItem>();
    private boolean mIsNewsAvailable = false;

    /* -------------------- Constructors -------------------- */

    public NewsModel()
    {
    }
    /* -------------------- Methods -------------------- */

    /**
     * Returns a list of all the news stored in the Model
     * @return the list of news
     */
    public ArrayList<NewsItem> getNews() {
        return mNews;
    }

    /**
     * Indicates if all News have been retrieved or not.
     * @return true if all news available have been fetched, false otherwise
     */
    public boolean isNewsAvailable()
    {
        return mIsNewsAvailable;
    }


    /**
     * Methods that gets back from the web the different News (Tweets, School News, etc)
     * and store it in the mNews attribute.
     */


    /**
     * fetch data from mSchoolNewsURL
     */
    public void fetchNews(){
        mIsNewsAvailable = false;
        final HttpClient httpclient = new DefaultHttpClient();
        String jsonResult = "";

        // HTTP POST Creation
        HttpPost httpPost = new HttpPost(mSchoolNewsURL);
        HttpResponse httpResponse = null;

        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

        // Sends HTTP POST Request
        try {
            httpResponse = httpclient.execute(httpPost);
            // get HTTP POST result
            jsonResult = EntityUtils.toString(httpResponse.getEntity());
        } catch (Exception e) {
            android.util.Log.e("ERROR", e.toString());
            e.printStackTrace();
        }

        try {
            JSONArray dataArray = new JSONArray(jsonResult);
            for(int i = 0 ; i < dataArray.length() ; i++){
                JSONObject currentObject =(JSONObject)dataArray.get(i);
                if(currentObject.getString("type").equals("News")) {
                    SchoolNews newsToAdd = new SchoolNews(currentObject.getString("title"), currentObject.getString("content"), new Date(currentObject.getString("date")));
                    mNews.add(newsToAdd);
                } else if(currentObject.getString("type").equals("Tweet")) {
                    Tweet tweetToAdd = new Tweet(currentObject.getString("content"), currentObject.getString("title"), new Date(currentObject.getString("date")));
                    mNews.add(tweetToAdd);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        mIsNewsAvailable = true;
    }
    /**
     * Delete all the News stored in the model
     */
    public void clearStoredNews(){
        mNews = new ArrayList<NewsItem>();
    }
}
