package com.eirbmmk.app.news;

import java.util.Date;

/**
 * Class to details news got from School Website.
 */
public class SchoolNews extends NewsItem {

    /**
     * News title
     */
    private String mTitle;

    /**
     * Constructor fo the SchoolNews
     *
     * @param title school news title
     * @param content school content title
     * @param date school news date
     */
    public SchoolNews(String title, String content, Date date){
        super(content, date);

        mTitle = title;
    }

    /**
     * Returns school news title
     * @return news title
     */
    public String getTitle() {
        return mTitle;
    }
}
