package com.eirbmmk.app.news;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eirbmmk.app.BaseSecondaryFragment;
import com.eirbmmk.app.R;

import java.text.SimpleDateFormat;


public class DetailedNewsFragment extends BaseSecondaryFragment {

	 /* -------------------- Attributes -------------------- */

    /**
     * The controller part of the MVC News pattern
     */
    private NewsController mController;

    /**
     * Item displayed in details in the fragment
     */
    private NewsItem mItem;

    /**
     * The rootView of the fragment layout
     */
    private View mRootView;


    /* -------------------- Constructors -------------------- */


    /**
     * Used by the Operating System when it killed the fragment to free memory
     * and need to recreate it.
     *
     * Never use this constructor directly !
     *
     */
    public DetailedNewsFragment()
    {
        // NEVER USE DIRECTLY THIS CONSTRUCTOR !
        // IT IS HERE ONLY FOR ANDROID BACKGROUND PROCESS (FRAGMENT RECREATION)
    }



    /**
     * Constructor of the DetailedNewsFragment class
     *
     * @param controller the news controller of the package
     * @param item the news to details.
     */
    public DetailedNewsFragment(NewsController controller, NewsItem item)
    {
        mController = controller;
        mItem = item;

        super.setBaseController(mController);
    }

    /* -------------------- Methods -------------------- */


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        // If the fragment is in an illegal state, don't continue, wait the recreation of it.
        if (super.onCreateView(inflater, container, savedInstanceState) == null)
            return null;


        mRootView = inflater.inflate(R.layout.fragment_detailed_news, container, false);

        TextView newsContent = (TextView) mRootView.findViewById(R.id.detailed_news_content);
        TextView newsTitle = (TextView) mRootView.findViewById(R.id.detailed_news_title);
        TextView newsDate = (TextView) mRootView.findViewById(R.id.detailed_news_date);
        ImageView newsImage = (ImageView) mRootView.findViewById(R.id.detailed_news_picture);

        newsContent.setText(mItem.getContent());
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEEE dd MMMM yyyy à HH:mm");
        newsDate.setText(dateFormat.format(mItem.getNewsDate()));

        if (mItem.getClass() == Tweet.class) {
            Tweet tweet = (Tweet) mItem;
            int imageResourceId = getResources().getIdentifier("ic_twitter", "drawable", mRootView.getContext().getPackageName());

            newsTitle.setText("@" + tweet.getAccount());
            newsImage.setImageResource(imageResourceId);
        } else if (mItem.getClass() == SchoolNews.class) {
            SchoolNews news = (SchoolNews) mItem;
            int imageResourceId = getResources().getIdentifier("ic_school_news_website", "drawable", mRootView.getContext().getPackageName());

            newsTitle.setText(news.getTitle());
            newsImage.setImageResource(imageResourceId);
        }
        return mRootView;
    }
}