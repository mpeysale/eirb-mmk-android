package com.eirbmmk.app.news;

import android.os.AsyncTask;

import com.eirbmmk.app.BaseController;
import com.eirbmmk.app.MainActivityController;
import com.eirbmmk.app.Screen;

import java.util.ArrayList;

/**
 * This class correspond to the controller part of the MVC design pattern for the news
 * functionality of the application.
 */
public class NewsController extends BaseController {

    /* -------------------- Attributes -------------------- */

    /**
    * Model part of the MVC design pattern
    */
    private NewsModel mModel;

    /**
    * View part of the MVC design pattern
    */
    private NewsFragment mFragment;


      /* -------------------- Constructors -------------------- */

    /**
     * Constructor of NewsController.
     *
     * @param container the mainActivity controller which controls the Activity containing all the fragments
     * @param model the model for the News functionality
     * @param fragment the view of the News functionality
     */
    public NewsController(MainActivityController container, NewsModel model, NewsFragment fragment)
    {
        super(container);
        mModel = model;
        mFragment = fragment;

        if (isNetworkConnected())
            getData();
    }

    /* -------------------- Public Methods -------------------- */

    /**
     * fetchs the news from the Model and asks the view to display it
     */
    public void getNews()
    {
        mFragment.showLoadingSpinner();

        if(!mModel.isNewsAvailable()) {
            AsyncTask<Void, Void, Void> waitData = new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... Params) {
                    if (!isCancelled()) {
                        while (!isCancelled() && !mModel.isNewsAvailable()) {
                            try {
                               Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void ok) { // not called if the AsyncTask is cancelled
                    displayNews();
                    mFragment.getSwipeRefreshLayout().setRefreshing(false);
                    mFragment.hideLoadingSpinner();

                    // remove the task to running tasks list
                    mAsyncRunningTasks.remove(this);


                }
            };

            // add the task to running tasks list
            mAsyncRunningTasks.add(waitData);

            // start the async task
            waitData.execute();
        } else {
            displayNews();
            mFragment.getSwipeRefreshLayout().setRefreshing(false);
            mFragment.hideLoadingSpinner();
        }
    }

    /**
     * Method to get News Data from different Web-services.
     *  Executes in an AsyncTask to let the UI responsive
     *
     */
    private void getData(){

        AsyncTask<Void, Void, Boolean> dataRequest = new AsyncTask<Void, Void, Boolean>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
               // mFragment.showLoadingSpinner();
            }

            @Override
            protected Boolean doInBackground(Void... Params) {
                if (!isCancelled()) {
                    mModel.fetchNews();
                }
                return true;
            }

            @Override
            protected void onPostExecute(Boolean ok) { // not called if the AsyncTask is cancelled

                // remove the task to running tasks list
                mAsyncRunningTasks.remove(this);
            }
        };

        // add the task to running tasks list
        mAsyncRunningTasks.add(dataRequest);

        // start the async task
        dataRequest.execute();
    }

    /**
     * Displays data in ListView
     */
    public void displayNews(){
       mFragment.displayNews(mModel.getNews());
    }

    /**
     * Displays the secondary fragment for showing specific news details
     *
     * @param item the item to display in detail
     */
    public void displayDetailedNewsFragment(NewsItem item)
    {
        // stop running tasks (because will try to display in the current fragment which will be replaced).
        stopAsyncRunningTasks();

        mMainActivityController.displaySecondaryFragment(new DetailedNewsFragment(this, item));
    }
}
