package com.eirbmmk.app.news;

import java.util.Date;

/**
 * Tweet for News functionality.
 * It details all tweet information.
 */
public class Tweet extends NewsItem {

    /**
     * Tweet owner
     */
    private String mAccount;

    /**
     * Constructor of the Tweet class
     *
     * @param content the tweet content
     * @param account the tweet account
     * @param date the tweet date
     */
    public Tweet(String content, String account, Date date){
        super(content, date);

        mAccount = account;
    }

    /**
     * Returns tweet content
     * @return returns content
     */
    public String getContent() {
        return mContent;
    }

    /**
     * Returns tweet account
     * @return the account
     */
    public String getAccount() { return mAccount; }
}

