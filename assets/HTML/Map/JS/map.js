var s;
var auth;

window.onload = function(){
   $.ajaxPrefilter( 'text', function( options ) { options.crossDomain = true; });
   s = Snap("#map");
   if(auth){
        loadFloorAuth("rdc");
   } else {
        loadFloor("rdc");
   }
}

function loadFloor(floor){
    Snap.load("SVG/" + floor + ".svg", function(data){
        $.ajax({
            url: "http://edt.eirb.fr/p/salles.json",
            method: "GET"
        }).done(function(rooms){
            s.clear();
            var dynamicRooms = data.selectAll("rect[title]");
            for(var i = 0 ; i < dynamicRooms.length ; i++){
                dynamicRooms[i].click(function(sender){
                    JavaMethods.roomClicked(sender.target.getAttribute("title"));
                });
            }

            dynamicRooms = data.selectAll("polygon[title]");
            for(var i = 0 ; i < dynamicRooms.length ; i++){
                dynamicRooms[i].click(function(sender){
                    JavaMethods.roomClicked(sender.target.getAttribute("title"));
                });
            }

            s.append(data);
        });

    });
}

function loadFloorAuth(floor){
    Snap.load("SVG/" + floor + ".svg", function(data){
        $.ajax({
            url: "http://edt.eirb.fr/p/salles.json",
            method: "GET"
        }).done(function(rooms){
            s.clear();
            var dynamicRooms = data.selectAll("rect[title]");
            for(var i = 0 ; i < dynamicRooms.length ; i++){
                dynamicRooms[i].click(function(sender){
                    JavaMethods.roomClicked(sender.target.getAttribute("title"));
                });
            }

            dynamicRooms = data.selectAll("polygon[title]");
            for(var i = 0 ; i < dynamicRooms.length ; i++){
                dynamicRooms[i].click(function(sender){
                    JavaMethods.roomClicked(sender.target.getAttribute("title"));
                });
            }

            for(var i = 0 ; i < rooms.length ; i++){
                var room =  data.select("rect[id='" + rooms[i].slug + "']") || data.select("polygon[id='" + rooms[i].slug + "']");
                if(room){
                    if(rooms[i].free){
                        room.attr({fill: "#39B54A"});
                    } else {
                        room.attr({fill: "#BF161C"});
                    }
                }
            }

            s.append(data);
        });

    });
}